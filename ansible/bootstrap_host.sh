#!/bin/zsh

# This script bootstraps a host with the metrics-common role, including initial
# setup of authentication for further provisioning.

set -e
umask 0022

ug_err()
{
	echo "${1}" 1>&2 && return ${2:-1}
}

usage()
{
	ug_err "usage: ${1##*/} hostname"
}

if [ "$#" -ne 1 ]; then
  usage $0
fi

/usr/local/bin/ansible-playbook --user admin --limit "$1" -i dev metrics-common.yml

